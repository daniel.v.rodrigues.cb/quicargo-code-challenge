angular.module('quicargo').service('mapService', mapService);

function mapService() {
  const ACCESS_TOKEN =
    'pk.eyJ1IjoiZGFuaWVyb2QiLCJhIjoiY2tqaWx1NmswNGgxaTJ6bnF6cjU0OWh1NCJ9.FrTqdnFVr3Nc9U5lKxQwug';

  _self = this;
  _self.markers = [null, null];

  return {
    map: _self.map,
    markers: _self.markers,
    createMapOn: createMapOn,
    addRouteStartAt: addRouteStartAt,
    addRouteFinishAt: addRouteFinishAt,
    fitToRoute: fitToRoute,
  };

  function createMapOn(elementId) {
    document.readyState !== 'complete'
      ? setTimeout(() => _initializeMap(elementId), 100)
      : _initializeMap(elementId);
  }

  function addRouteStartAt(address) {
    const startMarkerOptions = { color: '#2c9e2c' };
    _addRouteAt(address, 0, startMarkerOptions);
  }

  function addRouteFinishAt(address) {
    const finishMarkerOptions = { color: '#ee6b4f' };
    _addRouteAt(address, 1, finishMarkerOptions);
  }

  function fitToRoute() {
    if (_self.markers[0] !== null && _self.markers[1] !== null) {
      _self.map.fitBounds(
        [
          _self.markers[0].getLngLat().toArray(),
          _self.markers[1].getLngLat().toArray(),
        ],
        { padding: 50 }
      );
    }
  }

  function _initializeMap(elementId) {
    mapboxgl.accessToken = ACCESS_TOKEN;
    _self.map = new mapboxgl.Map({
      container: elementId,
      style: 'mapbox://styles/mapbox/streets-v11',
      center: _self._userLocation || [0, 0],
      zoom: 1,
      interactive: false,
    });

    _centerMapOnUserLocation();
  }

  function _centerMapOnUserLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        (position) =>
          (_self._userLocation = [
            position.coords.latitude,
            position.coords.longitude,
          ])
      );
    } else {
      _self._userLocation = [0, 0];
    }
  }

  async function _addRouteAt(address, index, options) {
    const feature = await _geocodeAddress(address);
    if (feature) {
      const marker = new mapboxgl.Marker(options).setLngLat(feature.center);

      _self.markers[index] && _self.markers[index].remove();

      _self.markers.splice(index, 1, marker);
      marker.addTo(_self.map);
      fitToRoute();
    }
  }

  async function _geocodeAddress(address) {
    var mapboxClient = mapboxSdk({ accessToken: ACCESS_TOKEN });
    const response = await mapboxClient.geocoding
      .forwardGeocode({
        query: address,
        autocomplete: false,
        limit: 1,
      })
      .send();

    if (
      response &&
      response.body &&
      response.body.features &&
      response.body.features.length
    ) {
      console.log(
        'Mapbox SDK for the query "%s" found the location "%s"',
        address,
        response.body.features[0].place_name
      );
      return response.body.features[0];
    }
  }
}
