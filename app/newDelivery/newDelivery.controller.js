angular
  .module('quicargo')
  .controller('newDeliveryController', NewDeliveryController);

NewDeliveryController.$inject = [
  '$scope',
  '$http',
  'countries',
  'goods',
  'quantities',
  'mapService',
];
/* @ngInject */
function NewDeliveryController(
  $scope,
  $http,
  countries,
  goods,
  quantities,
  mapService
) {
  $scope.selectCountryPlaceholder = 'Select a country';
  $scope.addressPlaceholder = 'Street door, city, zipcode';
  $scope.countries = countries.get();
  $scope.goodsTypes = goods.getTypes();
  $scope.quantities = quantities.get();
  $scope.newDelivery = {
    pickup: {
      country: null,
      address: null,
      date: null,
      from: null,
      to: null,
    },
    delivery: {
      country: null,
      address: null,
      date: null,
      from: null,
      to: null,
    },
    quantity: $scope.quantities[0],
    goodsType: $scope.goodsTypes[0],
    width: null,
    length: null,
    height: null,
    weight: null,
  };

  $scope.saveNewDelivery = () => {
    // TODO: implement save of new delivery after creating BE server
    $http.post('some-endpoint/new-delivery', $scope.newDelivery);
    console.log('Saving new delivery: ', $scope.newDelivery);
  };

  mapService.createMapOn('mapbox_map');

  $scope.$watch('newDelivery.pickup.country', function (newValue, oldValue) {
    if (newValue && newValue !== oldValue) {
      addRouteStartAt($scope.newDelivery.pickup.address, newValue.text);
    }
  });
  $scope.$watch('newDelivery.pickup.address', function (newValue, oldValue) {
    if (newValue && newValue !== oldValue) {
      addRouteStartAt(
        newValue,
        $scope.newDelivery.pickup.country &&
          $scope.newDelivery.pickup.country.text
      );
    }
  });

  $scope.$watch('newDelivery.delivery.country', function (newValue, oldValue) {
    if (newValue && newValue !== oldValue) {
      addRouteFinishAt($scope.newDelivery.delivery.address, newValue.text);
    }
  });
  $scope.$watch('newDelivery.delivery.address', function (newValue, oldValue) {
    if (newValue && newValue !== oldValue) {
      addRouteFinishAt(
        newValue,
        $scope.newDelivery.delivery.country &&
          $scope.newDelivery.delivery.country.text
      );
    }
  });

  let startTimer = null;
  function addRouteStartAt(address, country) {
    clearTimeout(startTimer);
    startTimer = setTimeout(doIt, 1000);

    function doIt() {
      mapService.addRouteStartAt(
        `${(address && address + ', ') || ''}${country || ''}`
      );
    }
  }

  let finishTimer = null;
  function addRouteFinishAt(address, country) {
    clearTimeout(finishTimer);
    finishTimer = setTimeout(doIt, 1000);

    function doIt() {
      mapService.addRouteFinishAt(
        `${(address && address + ', ') || ''}${country || ''}`
      );
    }
  }
}
