import newDeliveryTemplate from './newDelivery/new-delivery.html';

angular
  .module('quicargo')
  .config(['$urlRouterProvider', '$stateProvider', quicargoStateRouter]);

/* @ngInject */
function quicargoStateRouter($urlRouterProvider, $stateProvider) {
  $urlRouterProvider.otherwise('/new-delivery');
  $stateProvider.state('new-delivery', {
    name: 'new-delivery',
    url: '/new-delivery',
    template: newDeliveryTemplate,

    controller: 'newDeliveryController',
  });

  $stateProvider.state('my-deliveries', {
    name: 'my-deliveries',
    url: '/my-deliveries',
    template:
      '<div style="display:flex;flex-direction:column;align-items:center; justify-content:center; height:100%; gap:24px;"><h2>My Deliveries</h2><h3>Comming soon!</h3></div>',
  });

  $stateProvider.state('history', {
    name: 'history',
    url: '/history',
    template:
      '<div style="display:flex;flex-direction:column;align-items:center; justify-content:center; height:100%; gap:24px;"><h2>History</h2><h3>Comming soon!</h3></div>',
  });
}
