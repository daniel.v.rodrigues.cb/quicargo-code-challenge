angular.module('quicargo').factory('countries', countriesFactory);

/* @ngInject */
function countriesFactory() {
  return {
    get: function () {
      return [
        { value: 'de', text: 'Germany' },
        { value: 'nl', text: 'Netherlands' },
        { value: 'pt', text: 'Portugal' },
        { value: 'ch', text: 'Switzerland' },
      ];
    },
  };
}
