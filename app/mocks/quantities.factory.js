angular.module('quicargo').factory('quantities', quantitiesFactory);

/* @ngInject */
function quantitiesFactory() {
  return {
    get: function () {
      return Array.from({ length: 10 }, (_, i) => ({
        value: i + 1,
        text: `${i + 1} pallet${i + 1 > 1 ? 's' : ''}`,
      }));
    },
  };
}
