angular.module('quicargo').factory('goods', goodsFactory);

/* @ngInject */
function goodsFactory() {
  return {
    getTypes: function () {
      return [
        { value: 0, text: 'Paper' },
        { value: 1, text: 'Plastic' },
        { value: 2, text: 'Textil' },
        { value: 3, text: 'Other' },
      ];
    },
  };
}
