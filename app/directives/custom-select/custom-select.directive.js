import customSelectTemplate from './custom-select.directive.html';

angular.module('quicargo').directive('customSelect', customSelect);

function customSelect() {
  return {
    template: customSelectTemplate,
    replace: true,
    scope: {
      placeholder: '<',
      options: '<',
      selected: '=',
    },
  };
}
