import customInputTemplate from './custom-input.directive.html';

angular.module('quicargo').directive('customInput', customInput);

function customInput() {
  return {
    template: customInputTemplate,
    replace: true,
    scope: {
      placeholder: '<',
      label: '@',
      icon: '@',
      append: '@',
      model: '=',
    },
  };
}
