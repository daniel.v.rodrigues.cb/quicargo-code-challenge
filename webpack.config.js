const path = require('path');
const glob = require('glob');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = (args) => {
  const mode = args.dev ? 'development' : 'production';

  return {
    mode,
    entry: {
      index: glob.sync('./app/**/*.js'),
      style: glob.sync('./sass/main.scss'),
    },
    output: {
      publicPath: './',
      path: path.resolve(__dirname, 'dist'),
      sourceMapFilename: '[name].[id][hash:8].map',
      chunkFilename: '[id].[hash:8].js',
    },
    module: {
      rules: [
        {
          test: /app*\.js$/,
          use: [{ loader: 'babel-loader' }],
        },
        {
          test: /\.html$/i,
          loader: 'html-loader',
        },
        {
          test: /\.scss$/i,
          use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader'],
        },
      ],
    },
    plugins: [
      new HtmlWebpackPlugin({
        title: 'Quicargo Code Test',
        template: './index.html',
      }),
      new MiniCssExtractPlugin(),
    ],
    devServer: {
      publicPath: '/',
      contentBase: path.join(process.cwd(), 'dist'),
    },
    devtool: 'eval',
  };
};
