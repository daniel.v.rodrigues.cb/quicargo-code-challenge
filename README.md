# Quicargo Code Challenge

## Task planning <font size="2">(☑️ - Done | ☐ - TODO)</font>

- ☑️ Create boilerplate project with Webpack + SASS + ES6 + Tests
  - ☐ TODO: Missing Tests
- ☑️ Create pages routing with "comming soon" pages
- ☑️ Create and style the required HTML components;
  - ☐ TODO: Improve responsiveness
- ☑️ Create pages, required interactions and validations;
  - ☐ TODO: Add form validations in New Delivery page
  - ☐ TODO: Create My Deliveries page
  - ☐ TODO: Create History
- ☐ Implement a backend with Express + Sequelize
- ☐ Add Docker Compose to run everything together

<br>

## Development Node and NPM versions

```bash
node v12.18.3
npm 6.14.6
```

# Running the application

<!-- TODO: Add docker-compose after implementing BE
## Docker Compose <font size="2">(from root folder)</font>
-->

## Manual <font size="2">(from root folder)</font>

```bash
# install npm dependencies
npm install

# run in production mode
npm start

#open localhost:8080 in the browser
```

# TODO's

From the task planning at the top its clear that I have a few things I wanted to do in the scope of this code challenge.
But since I didn't want to take much more time I'm delivering it with what I believe is the most important stuff for the Frontend position.

- Add **unit** and **E2E** tests;
- Add form validation in the New Delivery page;
- Create API server (using Express.js + Sequelize)
- Document API using Open API Specification;
- Setup **docker-compose** to run the application.
- Implement the missing pages.
- Implement or add 3rd party **date/time picker** component
